\documentclass{mast}

\title{Season 4 Diagnostic}
\author{MAST}
\date{2021}

\begin{document}

\maketitle

\section{Rules and Instructions}

The MAST Diagnostic is a selection of problems the staff team has written that have appeared in various contests, along with original problems that have never been seen before. There are eight problems, two of each of the four major subjects in math contests.
\begin{itemize}
    \item There are 2 problems for each subject, ordered in roughly increasing difficulty. However, there are still some things you must prove: there must be a full and rigorous proof for a “find-all” style problem. Solution sketches, at a minimum, are still mandatory.
    \item You must indicate if you remember a past problem.
    \item If you think you’ve accidentally broken any of these rules, please let us know. The difference between cheating and a mistake is transparency.
    \item If you’ve deliberately broken any of these rules, please also let us know.
\end{itemize}

\subsection{Asking for Help}
If you’ve been stuck on a problem for a while and want a hint, you can ask us or other people. You may also consult static media. (This includes books, articles, and parts of the internet such as encyclopedias or other references. This does not include making a forum post on AoPS or StackExchange. You also may not look for the problem.) If you do ask other people, here are a couple of rules:
\begin{itemize}
	\item Do not ask any MAST applicants/potential applicants. \emph{Unlike last year, you can ask students for hints this year.}
    \item The person helping you should know that they are helping you on a diagnostic intended to gauge your ability. So they should not give you the entire solution and instead be giving helpful hints. This means that you should only ask people mature enough to understand this for hints.
    \item Don’t try to get the solution, try to get small hints. Just enough to get you moving forward on the problem.
    \item You must indicate to me that you have asked someone for help, and you must indicate to us how they have helped you. (Even if it wasn’t actually helpful in the end.)
\end{itemize}

\subsection{Seen Before}
Because these are mostly problems that’ve appeared elsewhere, some applicants may have seen some of the problems. If there is any way you may have been exposed to the problem before, such as taking JMC and/or reviewing the solutions packet, indicate this in your PDF submission. Something that might be obvious but should be explicitly stated anyway: if you intend to apply, you should not look at any of the problems after you’ve seen this document. Nor should you frantically review JMC/NARML/MAT to “prepare” for this application. You know what’s right and what’s not, so please don’t make yourself look stupid by claiming that something obviously unfair is ``technically not cheating.''

\section{Problems}

\subsection{Algebra}

\begin{enumerate}
    \item Tommy has $5$ red blocks and $5$ blue blocks, all of which have positive integer masses in grams, such that the red blocks can be arranged so that their masses form a strictly increasing arithmetic sequence and the blue blocks can be arranged so that their masses form a strictly increasing geometric sequence. The total mass of the blue blocks is equal to the total mass of the red blocks, and the red block of the least mass and the blue block of the least mass are equal in mass. Find the smallest possible total mass of the red blocks.
    \item Find the number of monic sixth degree polynomials $P(x)$ with integer coefficients between $0$ and $4,$ inclusive, such that exactly one of $P(1), P(2), P(3),$ and $P(4)$ does not leave a remainder of $P(0)$ when divided by $5.$
\end{enumerate}

\subsection{Combinatorics}

\begin{enumerate}
    \item The numbers ${1, 2, \dots , 8}$ are placed in each of the cells in the magic square below such that the number in each cell is distinct, and the sum of all numbers in each slanted row and each column is the same. Compute the number of ways that the magic square can be filled out.
    \begin{center}
        \begin{asy}
        size(3.75cm);
        pen cgreen = rgb("CCFFCC"), corange = rgb("FFCC99"), cpurple = rgb("E5CCFF"), cyellow = rgb("FFFFCC"), cblue = rgb("99CCFF");
        pen[] shapecolors = {
        cgreen, corange, cpurple, cyellow, cpurple,
        cblue, cpurple, cgreen, corange, cblue, cgreen,
        cyellow, cblue, cyellow,
        cgreen, cpurple, corange, corange,
        cyellow, cyellow, cpurple, cblue,
        cgreen,
        corange, cblue, cgreen, cpurple, cblue, cyellow,
        cgreen, cgreen,
        cyellow, corange,
        };
        filldraw((0,0)--(1,0)--(1,10/9)--(0,2/3)--cycle,corange);
        filldraw((1,0)--(2,0)--(2,14/9)--(1,10/9)--cycle,fuchsia*0.7+white);
        filldraw((1,10/9)--(1,23/9)--(2,3)--(2,14/9)--cycle,cblue);
        filldraw((2,14/9)--(3,2)--(3,3)--(2,3)--cycle,cyellow);
        filldraw((2,3)--(1,3)--(1,23/9)--cycle,cgreen);
        filldraw((1,10/9)--(0,2/3)--(0,19/9)--(1,23/9)--cycle,cpurple);
        filldraw((0,3)--(0,19/9)--(1,23/9)--(1,3)--cycle,rgb(255,64,64)*0.7+white);
        filldraw((2,0)--(3,0)--(3,2)--(2,14/9)--cycle,cgreen);
        
        draw((0,0)--(3,0)--(3,3)--(0,3)--cycle, linewidth(2.5));
        draw((0,2/3)--(3,2),linewidth(1.5));
        draw((0,19/9)--(2,3)--(2,0),linewidth(1.5));
        draw((1,3)--(1,0),linewidth(1.5));
        \end{asy}
    \end{center}
	\item There are six people attending a party. A \emph{clique} consists of three or more people that all know each other, and every partygoer belongs to at least one clique.\footnote{Here, ``knowing'' is a mutual relationship.} Every pair of people hold a conversation if and only if there is a clique they both belong to. If $9$ conversations are held, how many ways are there to choose a group of three people that all know each other? Give \emph{all} possible answers to this question.
\end{enumerate}

\subsection{Geometry}

\begin{enumerate}
    \item Let $ABCD$ be a square with side length $5$. Points $P$, $Q$, $R$, and $S$ lie on sides $\overline{AB},\overline{BC},\overline{CD},$ and $\overline{AD}$ respectively where $PQRS$ is a square with area $13.$ What is the maximum possible area of the quadrilateral formed by the pairwise intersections of lines $PC$, $QD$, $RA$, and $SB$?
    \item In $\triangle{ABC}$, $BC = 20$ and $M$ is the midpoint of $BC.$ Lines $\ell_1$ and $\ell_2$ pass through $B$ and $C$ respectively and are both perpendicular to line $AM.$ Find the smallest positive real $K$ such that for any $\triangle{ABC}$ with area $K$, at least one of the following is true: $\ell_1$ meets segment $AC$ or $\ell_2$ meets segment $AB.$
\end{enumerate}

\subsection{Number Theory}

\begin{enumerate}
    \item How many $5$-digit positive integer multiples of $3$ have more digits that are multiples of $3$ than digits that are not multiples of $3?$
    \item The function $\phi(n)$ denotes the number of positive integers less than or equal to $n$ that are relatively prime to $n.$ Determine the number of positive integers $a,$ relatively prime to $77,$ such that there exists some integer $n$ satisfying
			\[\frac{\phi(n)}{n}=\frac{a}{77}.\]
\end{enumerate}

\end{document}
